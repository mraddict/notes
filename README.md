<h1>
  我的笔记👻
  <img src="https://gitlab.com/mraddict/notes/badges/main/pipeline.svg" />
  <img src="https://github.com/MR-Addict/notes/actions/workflows/docker.yml/badge.svg?branch=main"/>
  <img src="https://github.com/MR-Addict/notes/actions/workflows/server.yml/badge.svg?branch=main"/>
</h1>

本项目基于mdbook框架，主要是用作笔记的功能，记录一些重要的内容，以方便我在不同的机器上使用Copy+Paste魔法。

因为是笔记的原因，所以我不会写得太详细，目的是达到我自己能看懂，日后有时间会逐渐完善更正相关内容。

你可以通过以下网址访问网页内容：

- [https://notes.mraddict.top](https://notes.mraddict.top)
- [https://mraddict.gitlab.io/notes](https://mraddict.gitlab.io/notes)
- [https://mr-addict.github.io/notes](https://mr-addict.github.io/notes)
