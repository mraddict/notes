# Summary

[我的笔记](intro.md)

- [树莓派](rpi/intro.md)

  - [配置 vim](rpi/vim.md)
  - [使用 ssh](rpi/ssh.md)
  - [配置 alias](rpi/aliases.md)
  - [使用 crontab](rpi/crontab.md)
  - [使用 wiringpi](rpi/wiringpi.md)
  - [使用 powershell](rpi/powershell.md)
  - [配置 oh-my-posh](rpi/oh-my-posh.md)

- [Linux](linux/intro.md)

  - [Linux 的 Swap 空间](linux/swap.md)
  - [Linux 用户和用户组](linux/users.md)
  - [其他配置及常用命令](linux/others.md)

- [Openwrt](openwrt/intro.md)

  - [使用 vim](openwrt/vim.md)
  - [使用 init](openwrt/init.md)
  - [使用 uhttpd](openwrt/uhttpd.md)
  - [其他常用命令](openwrt/others.md)

- [教程](tutorial/intro.md)

  - [使用 Git](tutorial/git.md)
  - [使用 Node](tutorial/node.md)
  - [使用 Nginx](tutorial/nginx.md)
  - [使用 MySQL](tutorial/mysql.md)
  - [使用 Docker](tutorial/docker.md)
  - [使用 Apache2](tutorial/apache2.md)
  - [使用 Clash 内核](tutorial/clash.md)
  - [使用 Wireguard](tutorial/wireguard.md)
  - [使用 GitHub Actions](tutorial/github-actions.md)

- [服务器搭建](server/intro.md)

  - [搭建 MQTT 服务器](server/mqtt.md)
  - [搭建 VSCode 服务器](server/vscode.md)
  - [搭建 Adguard 服务器](server/adguard.md)
  - [搭建 Wireguard 服务器](server/wireguard.md)
  - [搭建 NextCloud 服务器](server/nextcloud.md)
  - [搭建 Homeassistant 服务器](server/homeassistant.md)

- [React](react/intro.md)

  - [Tailwind](react/tailwind.md)

- [编程](programming/intro.md)
  - [SQL](programming/sql/intro.md)
    - [Hello World](programming/sql/hello-world.md)
    - [SELECT](programming/sql/select.md)
    - [WHERE](programming/sql/where.md)
    - [AND-OR-NOT](programming/sql/and-or-not.md)
  - [Lua](programming/lua/intro.md)
    - [Hello World](programming/lua/hello-world.md)
    - [变量](programming/lua/variable.md)
    - [运算符](programming/lua/operator.md)
    - [循环](programming/lua/while.md)
    - [函数](programming/lua/function.md)
    - [Math](programming/lua/math.md)
    - [Table](programming/lua/table.md)
    - [Class](programming/lua/class.md)
  - [Typescript](programming/typescript/intro.md)
    - [Hello World](programming/typescript/hello-world.md)
    - [Types](programming/typescript/types.md)
    - [Function](programming/typescript/function.md)
    - [Object](programming/typescript/object.md)
    - [Aliases](programming/typescript/aliases.md)
    - [Union](programming/typescript/union.md)
