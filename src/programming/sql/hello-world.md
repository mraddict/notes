# HELLO WORLD

## 一、示例

```sql
USE sql_store;

SELECT *
FROM customers
-- WHERE customer_id = 1
ORDER BY first_name
```

## 二、知识点

- `USE`使用DB
- `SELECT`选择Column
- `FROM`选择Table
- `WHERE`筛选
- `ORDER BY`排序
- `--`注释
