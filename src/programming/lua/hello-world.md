# Hello world

## 示例

print输出：

```lua
print("Hello world")
```

标准输出：

```lua
io.write("Size of string ", #"String", ".\n")
```
