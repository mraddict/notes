# 搭建Wireguard服务器

配置Docker容器：

```yaml
version: "3"
services:
  wg-easy:
    environment:
      - WG_HOST=[change-this]
      - PASSWORD=[change-this]
      - WG_DEFAULT_DNS=1.1.1.1
      - WG_MTU=1420

    image: weejewel/wg-easy
    container_name: wg-easy
    volumes:
      - ./data:/etc/wireguard
    ports:
      - 51820:51820/udp
      - 51821:51821/tcp
    restart: unless-stopped
    cap_add:
      - NET_ADMIN
      - SYS_MODULE
    sysctls:
      - net.ipv4.ip_forward=1
      - net.ipv4.conf.all.src_valid_mark=1
```

启动容器：

```bash
docker-compose up -d
```
