# 其他常用命令

更新包：

```bash
opkg update
```

查看系统信息：

```bash
cat /proc/cpuinfo
```

查看已连接的WiFi设备：

```bash
cat /tmp/dhcp.leases
```
