# 使用Node

```admonish info
官方文档：[node](https://github.com/nodesource/distributions/blob/master/README.md)
```

## 一、安装Node

最新的稳定版本16：

```bash
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash - && sudo apt-get install -y nodejs
```

最新的稳定版本18：

```bash
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - && sudo apt-get install -y nodejs
```

## 二、安装常用包

安装最新的npm：

```bash
npm install -g npm@latest
```

安装最新的yarn：

```bash
npm install -g yarn@latest
```

安装最新的nodemon：

```bash
npm install -g nodemon@latest
```

安装最新的hexo：

```bash
npm install -g hexo-cli@latest
```
